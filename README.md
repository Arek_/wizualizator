# Instalacja i uruchomienie
## Wersja Pythona: **3.6**
## Instalacja dodatkowych pakietów(wymaga uprawnień administratora)
```
pip install sklearn numpy flask json
```
## Uruchamianie
```
python .\Rest_Controller.py
```


# REST API do wizualizacji
## Pobranie wektora cech na podstawie korpusu

### Parametry wejściowe
* Metoda POST
* parametry w ścieżce: 
    * dimensions - liczba wymiarów do jakiej sprowadzamy podobienstwo
    * method - jaka metoda powinna byćuzyta do redukcji [isomap, locallyLinearEmbedding, MDS, spectralEmbedding, TSNE] - domyślnie isomap
* parametry w BODY:
    * korpus - plik z korpusem słów (format do dogadania)

### Przykład:
localhost:5000/dimensions=2&method=MDS


### Parametry wyjściowe
* Lista punktów w przestrzeni n-wymiarowej w formacie JSON

### Przykład
```json
[ 
   [2,5],
   [1,3],
   [0,0]
]
```

