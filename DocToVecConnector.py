import numpy


class DocToVecConnector:
    def getVector(self, file):
        """
        Funkacja łącząca z DocToVec Api i
        :param file: nieprzetworzony plik z zapytania
        :return: wektor cech zwrócony z DocToVecApi
        """
        korpus = self._convert_file(file)
        return self._get_vector(korpus)

    def _convert_file(self, file):
        """
        funkcja konwertująca plik do formatu wymaganego przez DOCTOVEC api
        :return:
        """
        return numpy.empty([1, 1])

    def _get_vector(self, korpus):
        """
        Funkcja pobiera wektor cech z DocToget_vectorec
        :param korpus:
        :return:
        """
