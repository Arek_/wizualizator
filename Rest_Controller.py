import json

import numpy
from flask import Flask, request, flash
from json_tricks import dumps

from DimensionReducer import DimensionReducer
from DocToVecConnector import DocToVecConnector

KORPUS = 'korpus'

app = Flask(__name__)
app.secret_key = "super secret key"

doc_to_vec_connector = DocToVecConnector()
dimension_reducer = DimensionReducer()


@app.route('/<dimensions>', methods=['POST'])
def getDimensions(dimensions):
    if KORPUS not in request.files:
        flash('No file part')
        return "No korpus file", 400
    else:
        file = request.files[KORPUS]
        vector = doc_to_vec_connector.getVector(file)
        final_vector = dimension_reducer.decrease_dimensions(vector, dimensions)
        return json.dumps(final_vector, cls=NumpyEncoder)


if __name__ == '__main__':
    app.run()


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)
